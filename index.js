// console.log('Hello, World');

//// JSON Objects ????
		// also used in other programming languages hence the name JSON
		// Core JS has a built in JSON object that contains methods for parsing JSON objects and converting strings into JS objects
		// JSON is used for serializing different data types
			// {
			// 	"propertyA": "valueA",
			// 	"propertB": "valueB"
			// }

// JSON Object sample
		// {
		// 	'city': 'Quezon City',
		// 	'province': 'Metro Manila',
		// 	'country': 'Philippines'
		// }

//// JSON Arrays
		// [
		// 	{
		// 		'city': 'Cebu City',
		// 		'province': 'Cebu',
		// 		'country': 'Philippines'
		// 	},
		// 	{
		// 	 	'city': 'Manila City',
		// 	 	'province': 'Metro Manila',
		// 	 	'country': 'Philippines'
		// 	}
		// ]

// JSON Methods
	// Json object contains method for parsing and converting data into stringified JSON
	let batchesArr = [
										{
											batchName: 'Batch X'
										},
										{
											batchName: 'Batch Y'
										}
									 ];
	console.log('The original array:');
	console.log(batchesArr);

	// The stringify method is used to convert JS objects into a string
	console.log('Result from stringify method:');
	let stringBatchesArr = JSON.stringify(batchesArr);
	console.log(stringBatchesArr);

	console.log(typeof stringBatchesArr);

	let data = JSON.stringify(
														{
															name: 'John',
															address: {
																city: 'Cebu City',
																province: 'Cebu'
															}
														}
													 );

//// Using stringify Method with variables ////
		// When information is stored in a variable and is not hard coded into an object that is being stingified, we can supply the value with a variable
		// let firstName = prompt('Enter name:');
		// let lastName = prompt('Enter last name:');
		// let age = prompt('How old are you?');
		// let address = {
		// 	city: prompt('Which city do you live in?'),
		// 	country: prompt('Which country does your city address belong to?')
		// };

		// let otherData = JSON.stringify({
		// 	firstName: firstName, // if same name with variable, pwede ra wala
		// 	lastName, // same ani
		// 	age, // and kani
		// 	address: address 
		// })
		// console.log(otherData);

//// Converting stringified JSON into JS Objects ////
// Objects are common data types used in application because of the complex data structures that can be created out of them
// Inofrmation is commonly sent to applications in stringified JSON and then converted back into objects
// This happens both for sending information to a backend application and sending information back to a frontend application
let batchesJSON = `[{"batchName":"Batch X"},{"batchName":"Batch Y"}]`;
let parseBatchesJSON = JSON.parse(batchesJSON);
console.log(parseBatchesJSON);

console.log(parseBatchesJSON[0]);

let stringifiedObject = `{
	'name': 'John',
	'age': '31',
	'address': {
		'city': 'Cebu City',
		'country': 'Philippines'
	}
}`
console.log(JSON.parse(stringifiedObject));